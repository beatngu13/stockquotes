/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.application.i18n;

import org.eclipse.osgi.util.NLS;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class Messages extends NLS {
	private static final String BUNDLE_NAME = 
			"org.bitbucket.beatngu13.stockquotesapp.i18n.messages_de_DE";
	
	public static String I18NAboutHandler_Title;
	public static String I18NAboutHandler_Description;
	
	public static String I18NMainPart_Index;
	public static String I18NMainPart_Source;
	public static String I18NMainPart_Filter;
	public static String I18NMainPart_PreviousPrice;
	public static String I18NMainPart_LastClosePrice;
	public static String I18NMainPart_Volume;
	public static String I18NMainPart_LastTradeTime;
	public static String I18NMainPart_Date;
	public static String I18NMainPart_CompanyShortName;
	public static String I18NMainPart_CompanyName;
	public static String I18NMainPart_CurrentPrice;
	public static String I18NMainPart_DayHighPrice;
	public static String I18NMainPart_DayLowPrice;
	public static String I18NMainPart_FetchData;

	public static String I18NPreferencesDialog_Title;
	public static String I18NPreferencesDialog_Subtitle;
	public static String I18NPreferencesDialog_Description;
	public static String I18NPreferencesDialog_HttpProxy;
	public static String I18NPreferencesDialog_Use;
	public static String I18NPreferencesDialog_ProxyName;
	public static String I18NPreferencesDialog_ProxyPort;
	public static String I18NPreferencesDialog_Username;
	public static String I18NPreferencesDialog_Password;
	
	public static String I18NQuitHandler_Title;
	public static String I18NQuitHandler_Description;
	
	static {
		// Initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {}
	
}
