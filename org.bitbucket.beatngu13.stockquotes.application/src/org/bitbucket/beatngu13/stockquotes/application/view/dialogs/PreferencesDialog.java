/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.application.view.dialogs;

import org.bitbucket.beatngu13.stockquotes.application.core.ProxyModel;
import org.bitbucket.beatngu13.stockquotes.application.core.StockQuotesAccess;
import org.bitbucket.beatngu13.stockquotes.application.i18n.Messages;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class PreferencesDialog extends TitleAreaDialog {

	private ProxyModel proxyModel = new ProxyModel();
	private StockQuotesAccess access;
	
	private DataBindingContext context;
	private Button enabledCheckBox;
	private Label proxyNameLabel;
	private Text proxyNameText;
	private Label proxyPortLabel;
	private Text proxyPortText;
	private Label usernameLabel;
	private Text usernameText;
	private Label passwordLabel;
	private Text passwordText;
	
	// TODO DI
	public PreferencesDialog(StockQuotesAccess access, Shell parentShell) {
		super(parentShell);
		
		this.access = access;
	}
	
	@Override
	protected Control createContents(Composite parent) {
		Control contents = super.createContents(parent);

		setTitleImage(getShell().getDisplay().getSystemImage(SWT.ICON_WORKING));
		getShell().setText(Messages.I18NPreferencesDialog_Title);
		setTitle(Messages.I18NPreferencesDialog_Subtitle);
		setMessage(Messages.I18NPreferencesDialog_Description, 
				IMessageProvider.INFORMATION);
		createBindings();
		
		return contents;
	}
	
	private void createBindings() {
		context = new DataBindingContext();
		
		createSingleBindings(enabledCheckBox, proxyNameLabel, proxyNameText, 
				proxyPortLabel, proxyPortText, usernameLabel, usernameText, 
				passwordLabel, passwordText);
	}
	
	@SuppressWarnings("unchecked")
	private void createSingleBindings(Button src, Control... dest) {
		IObservableValue<Boolean> srcObserv = 
				SWTObservables.observeSelection(src);
		
		UpdateValueStrategy<Boolean, Boolean> never  = 
				new UpdateValueStrategy<Boolean, Boolean>(
				UpdateValueStrategy.POLICY_NEVER);
		UpdateValueStrategy<Boolean, Boolean> update = 
				new UpdateValueStrategy<Boolean, Boolean>(
					UpdateValueStrategy.POLICY_UPDATE);
		
		for (Control control : dest) {
			IObservableValue<Boolean> destObserv = 
					SWTObservables.observeEnabled(control);
			context.bindValue(destObserv, srcObserv, never, update);
		}
	}
	
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, 
				IDialogConstants.OK_LABEL, true);
		createButton(parent, IDialogConstants.CANCEL_ID, 
				IDialogConstants.CANCEL_LABEL, false);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite contents = (Composite) super.createDialogArea(parent);
		
		Composite innerContents = new Composite(contents, SWT.NONE);
		innerContents.setLayout(new GridLayout(2, false));
		innerContents.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, 
				true));
		
		Label enabledLabel = new Label(innerContents, SWT.NONE);
		enabledLabel.setText(Messages.I18NPreferencesDialog_HttpProxy);
		enabledLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, 
				false));
		
		enabledCheckBox = new Button(innerContents, SWT.CHECK);
		enabledCheckBox.setText(Messages.I18NPreferencesDialog_Use);
		enabledCheckBox.setSelection(proxyModel.isEnabled());
		enabledCheckBox.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, 
				false));

		proxyNameLabel = new Label(innerContents, SWT.NONE);
		proxyNameLabel.setText(Messages.I18NPreferencesDialog_ProxyName);
		proxyNameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, 
				false));
		
		proxyNameText = new Text(innerContents, SWT.BORDER);
		proxyNameText.setText(proxyModel.getProxyName());
		proxyNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, 
				false));
		
		proxyPortLabel = new Label(innerContents, SWT.NONE);
		proxyPortLabel.setText(Messages.I18NPreferencesDialog_ProxyPort);
		proxyPortLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, 
				false));
		
		proxyPortText = new Text(innerContents, SWT.BORDER);
		proxyPortText.setText(String.valueOf(proxyModel.getProxyPort()));
		proxyPortText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, 
				false));
		
		usernameLabel = new Label(innerContents, SWT.NONE);
		usernameLabel.setText(Messages.I18NPreferencesDialog_Username);
		usernameLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, 
				false));
		
		usernameText = new Text(innerContents, SWT.BORDER);
		usernameText.setText(proxyModel.getUsername());
		usernameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, 
				false));
		
		passwordLabel = new Label(innerContents, SWT.NONE);
		passwordLabel.setText(Messages.I18NPreferencesDialog_Password);
		passwordLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, 
				false));
		
		passwordText = new Text(innerContents, SWT.BORDER | SWT.PASSWORD);
		passwordText.setText(proxyModel.getPassword());
		passwordText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, 
				false));
		
		return contents;
	}
	
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == IDialogConstants.OK_ID) {
			proxyModel.setEnabled(enabledCheckBox.getSelection());
			proxyModel.setProxyName(proxyNameText.getText());
			proxyModel.setProxyPort(Integer.parseInt(proxyPortText.getText()));
			proxyModel.setUsername(usernameText.getText());
			proxyModel.setPassword(passwordText.getText());
			
			access.setProxy(proxyModel.isEnabled() ? proxyModel.getProxyName() 
					: null, proxyModel.getProxyPort());
			access.setProxyAuthentication(proxyModel.getUsername(), 
					proxyModel.getPassword());
		}
		
		close();
	}

}
