/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.bitbucket.beatngu13.stockquotes.application.view.parts;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.bitbucket.beatngu13.stockquotes.application.core.StockQuotesAccess;
import org.bitbucket.beatngu13.stockquotes.application.i18n.Messages;
import org.bitbucket.beatngu13.stockquotes.application.table.labels.CompanyNameLabelProvider;
import org.bitbucket.beatngu13.stockquotes.application.table.labels.CompanyShortNameLabelProvider;
import org.bitbucket.beatngu13.stockquotes.application.table.labels.CurrentPriceLabelProvider;
import org.bitbucket.beatngu13.stockquotes.application.table.labels.DateLabelProvider;
import org.bitbucket.beatngu13.stockquotes.application.table.labels.DayHighPriceLabelProvider;
import org.bitbucket.beatngu13.stockquotes.application.table.labels.DayLowPriceLabelProvider;
import org.bitbucket.beatngu13.stockquotes.application.table.labels.LastClosePriceLabelProvider;
import org.bitbucket.beatngu13.stockquotes.application.table.labels.LastTradeTimeLabelProvider;
import org.bitbucket.beatngu13.stockquotes.application.table.labels.PreviousPriceLabelProvider;
import org.bitbucket.beatngu13.stockquotes.application.table.labels.VolumeLabelProvider;
import org.bitbucket.beatngu13.stockquotes.application.table.model.StockDataContentProvider;
import org.bitbucket.beatngu13.stockquotes.application.table.model.StockDataModel;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.di.Focus;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.osgi.service.event.Event;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class MainPart {

	private TableViewer tableViewer;
	
	@Inject
	private StockDataModel model;
	@Inject
	private StockQuotesAccess access;

	@PostConstruct
	public void createComposite(Composite parent) {
		parent.setLayout(new GridLayout(6, false));

		Label indexLabel = new Label(parent, SWT.NONE);
		indexLabel.setText(Messages.I18NMainPart_Index);
		indexLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, 
				false));
		
		Combo indexCombo = new Combo(parent, SWT.READ_ONLY);
		indexCombo.setItems(access.getAllStockIndexes());
		indexCombo.select(0);
		indexCombo.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, 
				false));
		
		Label filterLabel = new Label(parent, SWT.NONE);
		filterLabel.setText(Messages.I18NMainPart_Filter);
		filterLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, 
				false));
		
		Text filterText = new Text(parent, SWT.BORDER);
		filterText.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, 
				false));
		
		Label sourceLabel = new Label(parent, SWT.NONE);
		sourceLabel.setText(Messages.I18NMainPart_Source);
		sourceLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, 
				false));
		
		Combo sourceCombo = new Combo(parent, SWT.READ_ONLY);
		sourceCombo.setItems(access.getAllSourceNames());
		sourceCombo.select(0);
		sourceCombo.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, 
				false));
		
		createTable(parent);

		indexCombo.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				access.setCurrentStockIndex(((Combo) e.widget).getText());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
		
		// TODO Filter handling
		filterText.addModifyListener(new ModifyListener() {
			
			@Override
			public void modifyText(ModifyEvent e) {
				System.out.println(Messages.I18NMainPart_Filter);
			}
		});
		
		sourceCombo.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				access.setCurrentSource(((Combo) e.widget).getText());
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {}
		});
	}
	
	private void createTable(Composite parent) {
		GridData tableLayout = new GridData(GridData.FILL_BOTH);
		tableLayout.horizontalSpan = 6;
		
		tableViewer = new TableViewer(parent, SWT.FULL_SELECTION);
		tableViewer.setContentProvider(new StockDataContentProvider());
		
		Table table = tableViewer.getTable();
		table.setLayoutData(tableLayout);
		
		TableViewerColumn viewerColumn1 = new TableViewerColumn(tableViewer, 
				SWT.LEFT);
		TableColumn column1 = viewerColumn1.getColumn();
		viewerColumn1.setLabelProvider(new CompanyShortNameLabelProvider());
		column1.setText(Messages.I18NMainPart_CompanyShortName);
		column1.setWidth(50);
		
		TableViewerColumn viewerColumn2 = new TableViewerColumn(tableViewer, 
				SWT.LEFT);
		TableColumn column2 = viewerColumn2.getColumn();
		viewerColumn2.setLabelProvider(new CompanyNameLabelProvider());
		column2.setText(Messages.I18NMainPart_CompanyName);
		column2.setWidth(100);
		
		TableViewerColumn viewerColumn3 = new TableViewerColumn(tableViewer, 
				SWT.RIGHT);
		TableColumn column3 = viewerColumn3.getColumn();
		viewerColumn3.setLabelProvider(new CurrentPriceLabelProvider());
		column3.setText(Messages.I18NMainPart_CurrentPrice);
		column3.setWidth(75);
		
		TableViewerColumn viewerColumn4 = new TableViewerColumn(tableViewer, 
				SWT.RIGHT);
		TableColumn column4 = viewerColumn4.getColumn();
		viewerColumn4.setLabelProvider(new DayHighPriceLabelProvider());
		column4.setText(Messages.I18NMainPart_DayHighPrice);
		column4.setWidth(100);
		
		TableViewerColumn viewerColumn5 = new TableViewerColumn(tableViewer, 
				SWT.RIGHT);
		TableColumn column5 = viewerColumn5.getColumn();
		viewerColumn5.setLabelProvider(new DayLowPriceLabelProvider());
		column5.setText(Messages.I18NMainPart_DayLowPrice);
		column5.setWidth(100);
		
		TableViewerColumn viewerColumn6 = new TableViewerColumn(tableViewer, 
				SWT.RIGHT);
		TableColumn column6 = viewerColumn6.getColumn();
		viewerColumn6.setLabelProvider(new PreviousPriceLabelProvider());
		column6.setText(Messages.I18NMainPart_PreviousPrice);
		column6.setWidth(100);
		
		TableViewerColumn viewerColumn7 = new TableViewerColumn(tableViewer, 
				SWT.RIGHT);
		TableColumn column7 = viewerColumn7.getColumn();
		viewerColumn7.setLabelProvider(new LastClosePriceLabelProvider());
		column7.setText(Messages.I18NMainPart_LastClosePrice);
		column7.setWidth(75);
		
		TableViewerColumn viewerColumn8 = new TableViewerColumn(tableViewer, 
				SWT.RIGHT);
		TableColumn column8 = viewerColumn8.getColumn();
		viewerColumn8.setLabelProvider(new VolumeLabelProvider());
		column8.setText(Messages.I18NMainPart_Volume);
		column8.setWidth(100);
		
		TableViewerColumn viewerColumn9 = new TableViewerColumn(tableViewer, 
				SWT.LEFT);
		TableColumn column9 = viewerColumn9.getColumn();
		viewerColumn9.setLabelProvider(new LastTradeTimeLabelProvider());
		column9.setText(Messages.I18NMainPart_LastTradeTime);
		column9.setWidth(100);
		
		TableViewerColumn viewerColumn10 = new TableViewerColumn(tableViewer, 
				SWT.LEFT);
		TableColumn column10 = viewerColumn10.getColumn();
		viewerColumn10.setLabelProvider(new DateLabelProvider());
		column10.setText(Messages.I18NMainPart_Date);
		column10.setWidth(100);
		
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		tableViewer.setInput(model);
	}
	
	@Inject
	@Optional
	private void receive(@UIEventTopic(
			StockQuotesAccess.EVENT_REFRESH_FINISHED) Event event) {
		System.out.println("Refreshing table");
		tableViewer.refresh();
	}

	@Focus
	public void setFocus() {
		tableViewer.getTable().setFocus();
	}
	
}
