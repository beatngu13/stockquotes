/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.application.core;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.bitbucket.beatngu13.stockquotes.application.i18n.Messages;
import org.bitbucket.beatngu13.stockquotes.application.table.model.StockDataModel;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.di.UIEventTopic;
import org.osgi.service.event.Event;

import de.hska.iwii.stockquotes.net.IStockQuotes;
import de.hska.iwii.stockquotes.net.StockData;
import de.hska.iwii.stockquotes.net.simulation.StockQuotesSimulation;
import de.hska.iwii.stockquotes.net.yahoo.StockQuotesYahoo;

/**
 * @author danielkraus1986@gmail.com
 *
 */
@Creatable
@Singleton
public class StockQuotesAccess {
	
	private enum RequestMode {
		SYNCH,
		ASYNCH
	}
	
	public static final String EVENT_REFRESH_FINISHED = 
			"org/bitbucket/beatngu13/stockquotesapp/refresh";
	
	private final RequestMode mode = RequestMode.ASYNCH;
	private String currentStockIndex = IStockQuotes.ALL_STOCK_INDEXES[0];
	private IStockQuotes currentSource;
	private Job job;
	
	@Inject
	private StockQuotesSimulation simulation;
	@Inject
	private StockQuotesYahoo yahoo;
	@Inject
	private StockDataModel model;
	@Inject
	private IEventBroker broker;
	
	@PostConstruct
	private void init() {
		currentSource = simulation;
		Timer timer = new Timer(true);
		
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				refresh();
			}
		}, 1_000, 10_000);
	}

	public void setCurrentSource(String srcName) {
		if (srcName.equals(simulation.getName())) {
			currentSource = simulation;
		} else {
			currentSource = yahoo;
		}
		
		System.out.println("New source: " + srcName);
		refresh();
	}
	
	public void setProxy(String inetAddr, int port) {
		yahoo.setProxy(inetAddr, port);
		System.out.println("Proxy: " + inetAddr + ":" + port);
	}

	public void setProxyAuthentication(String user, String password) {
		yahoo.setProxyAuthentication(user, password);
		System.out.println("Proxy authentication: " + user);
	}
	
	public void refresh() {
		System.out.println("Requesting data (" + mode.toString().toLowerCase() 
				+ ")");
		
		if (mode == RequestMode.ASYNCH) {
			requestDataAsynch();
		} else {
			job = new Job(Messages.I18NMainPart_FetchData) {
				
				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try {
						monitor.beginTask(
								StockQuotesAccess.this.mode.toString(), 100);

						model.data = requestDataSync();
							
						broker.post(EVENT_REFRESH_FINISHED, model.data);
					} catch (Exception e) {
						monitor.setCanceled(true);
						
						return Status.CANCEL_STATUS;
					}
					
					monitor.done();
					
					return Status.OK_STATUS;
				}
			};
			
			job.setUser(false);
			job.schedule();
		}
	}

	private List<StockData> requestDataSync() {
		List<StockData> data = null;
		
		try {
			data = currentSource.requestDataSync(currentStockIndex);
		} catch (IOException e) {
			System.out.println("Could not fetch data from source");
			e.printStackTrace();
		}
		
		return data;
	}

	private void requestDataAsynch() {
		currentSource.requestDataAsync(currentStockIndex);
	}
	
	@SuppressWarnings("unchecked")
	@Inject
	@Optional
	private void receive(
			@UIEventTopic(IStockQuotes.EVENT_TOPIC) Event event) {
		model.data = (List<StockData>) event
				.getProperty(IStockQuotes.EVENT_STOCK_DATA);
		
		broker.post(EVENT_REFRESH_FINISHED, model.data);
	}

	public String[] getAllSourceNames() {
		return new String[]{simulation.getName(), yahoo.getName()};
	}

	public String[] getAllStockIndexes() {
		return IStockQuotes.ALL_STOCK_INDEXES;
	}
	
	public String getCurrentStockIndex() {
		return currentStockIndex;
	}
	
	public void setCurrentStockIndex(String stockIndex) {
		currentStockIndex = stockIndex;
		
		System.out.println("New stock index: " + stockIndex);
		refresh();
	}
	
	@PreDestroy
	public void dispose() {
		if (job != null && job.getState() != Job.NONE) {
			job.cancel();
			
			try {
				job.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
