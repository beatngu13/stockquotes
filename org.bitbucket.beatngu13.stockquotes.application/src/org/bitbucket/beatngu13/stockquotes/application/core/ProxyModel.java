/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.application.core;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class ProxyModel {
	
	private boolean enabled = false;
	private String proxyName = "proxy.hs-karlsruhe.de";
	private int proxyPort = 8888;
	private String username = "";
	private String password = "";
	
	/**
	 * @return {@link #enabled}.
	 */
	public boolean isEnabled() {
		return enabled;
	}
	
	/**
	 * @param {@link #enabled}.
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	/**
	 * @return {@link #proxyName}.
	 */
	public String getProxyName() {
		return proxyName;
	}
	
	/**
	 * @param {@link #proxyName}.
	 */
	public void setProxyName(String proxyName) {
		this.proxyName = proxyName;
	}
	
	/**
	 * @return {@link #proxyPort}.
	 */
	public int getProxyPort() {
		return proxyPort;
	}
	
	/**
	 * @param {@link #proxyPort}.
	 */
	public void setProxyPort(int proxyPort) {
		this.proxyPort = proxyPort;
	}
	
	/**
	 * @return {@link #username}.
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * @param {@link #username}.
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	
	/**
	 * @return {@link #password}.
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * @param {@link #password}.
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
}
