/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.application.handlers;

import javax.inject.Inject;
import javax.inject.Named;

import org.bitbucket.beatngu13.stockquotes.application.core.StockQuotesAccess;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.IWorkbench;
import org.eclipse.swt.widgets.Shell;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class RefreshHandler {
	
	@Inject
	private StockQuotesAccess access;
	
	@Execute
	public void execute(IWorkbench workbench,
			@Named(IServiceConstants.ACTIVE_SHELL) Shell shell){
		access.refresh();
	}
	
}
