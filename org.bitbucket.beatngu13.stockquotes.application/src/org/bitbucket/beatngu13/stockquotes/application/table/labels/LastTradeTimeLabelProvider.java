/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.application.table.labels;

import java.util.Date;

import org.bitbucket.beatngu13.stockquotes.application.table.model.Formatters;
import org.eclipse.jface.viewers.ColumnLabelProvider;

import de.hska.iwii.stockquotes.net.StockData;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class LastTradeTimeLabelProvider extends ColumnLabelProvider {
	
	@Override
	public String getText(Object element) {
		Date content = ((StockData) element).getLastTradeTime();
		
		return content != null ? Formatters.dateFormatter.format(content)
				: "N/A";
	}
	
}
