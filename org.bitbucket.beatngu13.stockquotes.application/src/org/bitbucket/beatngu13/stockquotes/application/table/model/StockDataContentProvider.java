/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.application.table.model;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class StockDataContentProvider implements IStructuredContentProvider {

	@Override
	public void dispose() {}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {}

	@Override
	public Object[] getElements(Object inputElement) {
		return ((StockDataModel) inputElement).data.toArray();
	}

}
