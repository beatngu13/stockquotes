/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.application.table.labels;

import org.bitbucket.beatngu13.stockquotes.application.table.model.Formatters;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;

import de.hska.iwii.stockquotes.net.StockData;
import de.hska.iwii.stockquotes.net.StockData.CurrentPriceChange;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class CurrentPriceLabelProvider extends ColumnLabelProvider {
	
	@Override
	public String getText(Object element) {
		Double content = ((StockData) element).getCurrentPrice();
		
		return content != null ? Formatters.numberFormatter.format(content)
				: "N/A";
	}
	
	@Override
	public Color getBackground(Object element) {
		CurrentPriceChange priceChange = ((StockData) element).getPriceChange();
		Color backgroundColor;
		
		switch(priceChange) {
		case UP:
			backgroundColor = Display.getCurrent()
				.getSystemColor(SWT.COLOR_GREEN);
			break;
		case DOWN:
			backgroundColor = Display.getCurrent()
				.getSystemColor(SWT.COLOR_RED);
			break;
		default:
			backgroundColor = Display.getCurrent()
				.getSystemColor(SWT.COLOR_WHITE);
		}
		
		return backgroundColor;
	}
	
}
