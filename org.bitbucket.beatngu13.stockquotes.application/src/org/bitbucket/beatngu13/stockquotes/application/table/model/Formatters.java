/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.application.table.model;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class Formatters {
	
	public static NumberFormat numberFormatter = 
			NumberFormat.getCurrencyInstance();
	public static DateFormat dateFormatter = DateFormat.getInstance();
	
	static {
		numberFormatter.setCurrency(Currency.getInstance(Locale.US));
	}
}
