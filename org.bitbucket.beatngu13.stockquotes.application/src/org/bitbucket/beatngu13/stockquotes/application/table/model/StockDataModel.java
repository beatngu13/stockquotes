/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.application.table.model;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;

import de.hska.iwii.stockquotes.net.StockData;

/**
 * @author danielkraus1986@gmail.com
 *
 */
@Creatable
@Singleton
public class StockDataModel {
	
	public List<StockData> data = new ArrayList<StockData>();

}
