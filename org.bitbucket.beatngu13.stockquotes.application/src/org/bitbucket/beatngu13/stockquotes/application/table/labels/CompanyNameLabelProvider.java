/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.application.table.labels;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import de.hska.iwii.stockquotes.net.StockData;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class CompanyNameLabelProvider extends ColumnLabelProvider {
	
	public String getText(Object element) {
		return ((StockData) element).getCompanyName();
	}
	
}
