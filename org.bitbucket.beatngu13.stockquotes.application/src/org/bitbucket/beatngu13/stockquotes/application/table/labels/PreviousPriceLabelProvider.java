/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.application.table.labels;

import org.bitbucket.beatngu13.stockquotes.application.table.model.Formatters;
import org.eclipse.jface.viewers.ColumnLabelProvider;

import de.hska.iwii.stockquotes.net.StockData;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class PreviousPriceLabelProvider extends ColumnLabelProvider {
	
	@Override
	public String getText(Object element) {
		Double content = ((StockData) element).getPreviousPrice();
		
		return content != null ? Formatters.numberFormatter.format(content)
				: "N/A";
	}
	
}
