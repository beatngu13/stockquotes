/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.access;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

import de.hska.iwii.stockquotes.net.IStockQuotes;

/**
 * @author danielkraus1986@gmail.com
 * 
 */
public class StockQuotesTracker extends
		ServiceTracker<IStockQuotes, IStockQuotes> {
	
	private IStockQuotesAccess stockQuotesAccess;
	
	public StockQuotesTracker(BundleContext context, 
			IStockQuotesAccess stockQuotesAccess) {
		super(context, IStockQuotes.class.getName(), null);
		this.stockQuotesAccess = stockQuotesAccess;
	}
	
	public IStockQuotes addingService(ServiceReference<IStockQuotes> reference) {
		IStockQuotes service = super.addingService(reference);
		
		stockQuotesAccess.addSource(service);
		
		return service;
	}
	
	public void removedService(ServiceReference<IStockQuotes> reference, 
			IStockQuotes service) {
		super.removedService(reference, service);
		stockQuotesAccess.removeSource(service);
	}

}
