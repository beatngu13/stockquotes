package org.bitbucket.beatngu13.stockquotes.access;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

public class Activator implements BundleActivator {
	
	private ServiceRegistration<?> registration;

	public void start(BundleContext context) throws Exception {
		IStockQuotesAccess stockQuotesAccess = new StockQuotesAccess();
		StockQuotesTracker tracker = 
				new StockQuotesTracker(context, stockQuotesAccess);
		
		tracker.open();
		
		registration = context.registerService(
				IStockQuotesAccess.class.getName(), stockQuotesAccess, null);
	}
	
	public void stop(BundleContext context) throws Exception {
		registration.unregister();
	}

}
