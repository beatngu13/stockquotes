/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.access;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import de.hska.iwii.stockquotes.net.IStockQuotes;
import de.hska.iwii.stockquotes.net.StockData;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class StockQuotesAccess implements IStockQuotesAccess {
	
	private HashMap<String, IStockQuotes> sources = new HashMap<>(2, 1.0F);
	private IStockQuotes currentSource;

	@Override
	public void addSource(IStockQuotes src) {
		sources.put(src.getName(), src);
		System.out.println("Source added: " + src.getName());
		
		if (currentSource == null) {
			currentSource = src;
			
			System.out.println("New current source: " + src.getName());
		}
	}

	@Override
	public void removeSource(IStockQuotes src) {
		sources.remove(src.getName());
		System.out.println("Source removed: " + src.getName());
		
		if (currentSource.getName().equals(src.getName())) {
			currentSource = null;
			Iterator<IStockQuotes> iter = sources.values().iterator();
			
			if (iter.hasNext()) {
				currentSource = iter.next();
				
				System.out.println("New current source: " 
						+ currentSource.getName());
			} else {
				System.out.println("No current source available");
			}
		}
	}

	@Override
	public String toggleCurrentSource() {
		Collection<IStockQuotes> srcs = sources.values();
		String currentSourceName = currentSource.getName();
		String newSourceName = null;
		
		for (IStockQuotes stockQuotes : srcs) {
			if (!stockQuotes.getName().equals(currentSourceName)) {
				currentSource = stockQuotes;
				newSourceName = stockQuotes.getName();
				
				stockQuotes.reset();
				
				break;
			}
		}
		
		return newSourceName;
	}

	@Override
	public List<String> getAllSourceNames() {
		return new ArrayList<String>(sources.keySet());
	}

	@Override
	public String getCurrentSourceName() {
		return currentSource.getName();
	}

	@Override
	public boolean setCurrentSource(String srcName) {
		boolean success = false;
		
		if (sources.containsKey(srcName)) {
			currentSource = sources.get(srcName);
			success = true;
			
			currentSource.reset();
			System.out.println("New current source: " + srcName);
		} else {
			System.out.println("No such source available: " + srcName);
		}
		
		return success;
	}

	@Override
	public void setProxy(String inetAddr, int port) {
		Collection<IStockQuotes> srcs = sources.values();
		
		for (IStockQuotes stockQuotes : srcs) {
			stockQuotes.setProxy(inetAddr, port);
		}
	}

	@Override
	public void setProxyAuthentication(String user, String password) {
		Collection<IStockQuotes> srcs = sources.values();
		
		for (IStockQuotes stockQuotes : srcs) {
			stockQuotes.setProxyAuthentication(user, password);
		}
	}
	
	@Override
	public List<StockData> requestDataSync(String stockIndex) {
		List<StockData> data = null;
		
		try {
			data = currentSource.requestDataSync(stockIndex);
		} catch (IOException e) {
			System.err.println("Could not fetch data from source");
			e.printStackTrace();
		}
		
		return data;
	}

}
