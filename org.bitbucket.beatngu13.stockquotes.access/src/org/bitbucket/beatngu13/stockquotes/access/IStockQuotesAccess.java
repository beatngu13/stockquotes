/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.access;

import java.util.List;

import de.hska.iwii.stockquotes.net.IStockQuotes;
import de.hska.iwii.stockquotes.net.StockData;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public interface IStockQuotesAccess {
	
	public void addSource(IStockQuotes src);

	public void removeSource(IStockQuotes src);

	public String toggleCurrentSource();

	public List<String> getAllSourceNames();

	public String getCurrentSourceName();

	public boolean setCurrentSource(String srcName);

	public void setProxy(String inetAddr, int port);

	public void setProxyAuthentication(String user, String password);

	public List<StockData> requestDataSync(String stockIndex);

}
