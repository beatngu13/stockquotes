/**
 * 
 */
package org.bitbucket.beatngu13.stockquotes.printer;

import java.util.List;

import org.bitbucket.beatngu13.stockquotes.access.IStockQuotesAccess;

import de.hska.iwii.stockquotes.net.StockData;

/**
 * @author danielkraus1986@gmail.com
 *
 */
public class StockQuotesPrinter {
	
	private IStockQuotesAccess access;
	
	public StockQuotesPrinter(IStockQuotesAccess access) {
		this.access = access;
	}
	
	public void print(String stockIndex) {
		System.out.println("### Starting printing stock data ###");
		
		List<StockData> data = access.requestDataSync(stockIndex);
		
		for (StockData entity : data) {
			String companyName = entity.getCompanyName();
			String highPrice = entity.getDayHighPrice().toString();
			System.out.println(companyName + " | Day high price: " + highPrice);
		}
		
		System.out.println("### Finished printing stock data ###");
	}

	/**
	 * @return {@link #access}.
	 */
	public IStockQuotesAccess getAccess() {
		return access;
	}

	/**
	 * @param {@link #access}.
	 */
	public void setAccess(IStockQuotesAccess access) {
		this.access = access;
	}
	
}
