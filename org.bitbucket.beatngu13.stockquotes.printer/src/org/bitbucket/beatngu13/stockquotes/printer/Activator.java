package org.bitbucket.beatngu13.stockquotes.printer;

import org.bitbucket.beatngu13.stockquotes.access.IStockQuotesAccess;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import de.hska.iwii.stockquotes.net.IStockQuotes;

public class Activator implements BundleActivator {

	public void start(BundleContext context) throws Exception {
		ServiceReference<?> ref = 
				context.getServiceReference(IStockQuotesAccess.class.getName());
		IStockQuotesAccess access = 
				(IStockQuotesAccess) context.getService(ref);
		StockQuotesPrinter printer = new StockQuotesPrinter(access);
		
		printer.print(IStockQuotes.DOW_JONES);
		access.toggleCurrentSource();
		printer.print(IStockQuotes.DOW_JONES);
	}
	
	public void stop(BundleContext context) throws Exception {
		
	}

}
